package com.fnummer.fnummer;

import java.util.Scanner;

public class Fodselsnummer {

    private static FnumValidator FnumVal;

    public static void main(String[] args) {

        System.out.println("Skriv ditt fødselsnummer");
        Scanner scanner = new Scanner( System.in );
        String FNUM = scanner.nextLine();
        FnumVal = new FnumValidator(FNUM);

        if (FnumVal.isValid()) {
            System.out.println("Fodselsnummer er riktig");
            System.out.println("Fødselsår: " + FnumVal.getBirthYear());
            System.out.println("Fødselsmåned: " + FnumVal.getBirthMonth());
            System.out.println("Fødselsdag " + FnumVal.getBirthDay());
            System.out.println("Kjønn " + FnumVal.getSex());
        }
        else {
            System.out.println("Fødselsnummer er ikke gyldig");
        }
    }
}

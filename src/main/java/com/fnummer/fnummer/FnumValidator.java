package com.fnummer.fnummer;

public class FnumValidator {

    private byte FNUM[] = new byte[11];
    private boolean valid = false;

    public FnumValidator(String FNUMNumber) {
        if (FNUMNumber.length() != 11){
            valid = false;
        }
        else {
            for (int i = 0; i < 11; i++){
                FNUM[i] = Byte.parseByte(FNUMNumber.substring(i, i+1));
            }
            if (checkSum1() && checkSum2() && checkMonth() && checkDay()) {
                valid = true;
            }
            else {
                valid = false;
            }
        }
    }

    public boolean isValid() {
        return valid;
    }

    public int getBirthYear() {
        int year;
        int indiv;
        year = 10 * FNUM[4];
        year += FNUM[5];
        indiv = 100 * FNUM[6];
        indiv += 10 * FNUM[7];
        indiv += FNUM[8];

        if (indiv > 0 && indiv < 500) {
            year += 1900;
        }
        else if (indiv > 499 && indiv < 750 && year > 53 && year < 100) {
            year += 1800;
        }
        else if (indiv > 499 && indiv < 1000 && year >= 0 && year < 40) {
            year += 2000;
        }
        else if (indiv > 899 && indiv < 1000 && year >= 40 && year < 100) {
            year += 1900;
        }
        return year;
    }

    public int getBirthMonth() {
        int month;
        month = 10 * FNUM[2];
        month += FNUM[3];
        return month;
    }


    public int getBirthDay() {
        int day;
        day = 10 * FNUM[0];
        day += FNUM[1];
        return day;
    }

    public String getSex() {
        if (valid) {
            if (FNUM[8] % 2 == 1) {
                return "Mann";
            }
            else {
                return "Kvinne";
            }
        }
        else {
            return "---";
        }
    }

    private boolean checkSum1() {
        int sum1 = 3 * FNUM[0] +
                7 * FNUM[1] +
                6 * FNUM[2] +
                1 * FNUM[3] +
                8 * FNUM[4] +
                9 * FNUM[5] +
                4 * FNUM[6] +
                5 * FNUM[7] +
                2 * FNUM[8];
        sum1 %= 11;
        sum1 = 11 - sum1;

        if (sum1 == 11 && FNUM[9] == 0) {
            return true;
        }
        else if (FNUM[9] == sum1 && sum1 != 10) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean checkSum2() {
        int sum2 = 5 * FNUM[0] +
                4 * FNUM[1] +
                3 * FNUM[2] +
                2 * FNUM[3] +
                7 * FNUM[4] +
                6 * FNUM[5] +
                5 * FNUM[6] +
                4 * FNUM[7] +
                3 * FNUM[8] +
                2 * FNUM[9];
        sum2 %= 11;
        sum2 = 11 - sum2;

        if (sum2 == 11 && FNUM[10] == 0) {
            return true;
        }
        else if (FNUM[10] == sum2 && sum2 != 10) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean checkMonth() {
        int month = getBirthMonth();
        int day = getBirthDay();
        if (month > 0 && month < 13) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean checkDay() {
        int year = getBirthYear();
        int month = getBirthMonth();
        int day = getBirthDay();
        if ((day > 0 && day < 32) &&
                (month == 1 || month == 3 || month == 5 ||
                        month == 7 || month == 8 || month == 10 ||
                        month == 12)) {
            return true;
        }
        else if ((day > 0 && day < 31) &&
                (month == 4 || month == 6 || month == 9 ||
                        month == 11)) {
            return true;
        }
        else if ((day >0 && day < 30 && leapYear(year)) ||
                (day >0 && day < 29 && !leapYear(year))) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean leapYear(int year) {
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
            return true;
        else
            return false;
    }
}
